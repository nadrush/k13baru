//
//  ViewController.swift
//  k13
//
//  Created by Nadeem Abdur Rasheed on 30/01/19.
//  Copyright © 2019 Nadeem Abdur Rasheed. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    //LOGIN
    @IBOutlet weak var txtPasswd: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    
    @IBAction func btnLoginPressed(_ sender: Any) {
        if txtUsername.text != "" && txtPasswd.text != ""
        {
            //            performSegue(withIdentifier: "toHome", sender: self)
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "Username and password must be filled", preferredStyle: .alert); alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in alert.dismiss(animated: true, completion: nil)}))
            present(alert,animated: true,completion: nil)
        }
    }
    
    //SIGN UP - Parents
    @IBOutlet weak var txtParentsName: UITextField!
    @IBOutlet weak var txtParentsHP: UITextField!
    
    @IBOutlet weak var txtParentsEmail: UITextField!
    
    //SIGN UP - KIDS
    @IBOutlet weak var txtKidsName: UITextField!
    @IBOutlet weak var txtKidsDOB: UITextField!
    
    @IBOutlet weak var txtKidsSchool: UITextField!
    @IBOutlet weak var txtKidsGrades: UITextField!
    
    
    @IBAction func btnSignUpPressed(_ sender: Any) {
        if txtParentsName.text != "" && txtParentsHP.text != "" && txtParentsEmail.text != "" && txtKidsName.text != "" && txtKidsDOB.text != "" && txtKidsSchool.text != "" && txtKidsGrades.text != ""
        {
            
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "All textfield must be filled", preferredStyle: .alert); alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in alert.dismiss(animated: true, completion: nil)}))
            present(alert,animated: true,completion: nil)
        }
    }
    // SIGN UP #2
    @IBOutlet weak var txtSignUpUsername: UITextField!
    @IBOutlet weak var txtSignUpPass: UITextField!
    
    @IBOutlet weak var txtSignUpPass2: UITextField!
    
    @IBAction func btnSignUp2Pressed(_ sender: Any) {
        if txtSignUpUsername.text != "" && txtSignUpPass.text != "" && txtSignUpPass2.text != ""
        {
            if (txtSignUpPass.text == txtSignUpPass2.text)
            {
             //            performSegue(withIdentifier: "toHome", sender: self)
            }
            else
            {
                let alert = UIAlertController(title: "Alert", message: "Repeated Password is Incorrect", preferredStyle: .alert); alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in alert.dismiss(animated: true, completion: nil)}))
                present(alert,animated: true,completion: nil)
            }
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Username and password must be filled", preferredStyle: .alert); alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in alert.dismiss(animated: true, completion: nil)}))
            present(alert,animated: true,completion: nil)
        }
    }
    
    // GAME #1
    @IBOutlet weak var txtGameInput: UITextField!

    @IBOutlet weak var toGame2: UIButton!
    
    @IBAction func txtGameAns(_ sender: UITextField) {
        if (txtGameInput.text == "Cloudy")
        {
            toGame2.isEnabled = true
        }
    }
    
    //GAME #2
    
    @IBOutlet weak var lblGame2Ans: UILabel!
    
    @IBOutlet weak var toGame3: UIButton!
    @IBOutlet weak var txtGame2Input: UITextField!
    @IBAction func txtGame2Ans(_ sender: UITextField) {
        if(txtGame2Input.text == "Siapakah namamu ?")
        {
            lblGame2Ans.isHidden = false
            toGame3.isEnabled = true
        }
    }
    
    //GAME #3
    
    @IBOutlet weak var toGame4: UIButton!
    @IBOutlet weak var txtGameF: UITextField!
    @IBOutlet weak var txtGameJ: UITextField!
    @IBOutlet weak var txtGameN: UITextField!
    @IBOutlet weak var txtGameV: UITextField!
    
    @IBAction func txtGame3InputF(_ sender: UITextField) {
        if(txtGameF.text == "F")
        {
            txtGameF.isHidden = true
        }
    }
    @IBAction func txtGame3InputJ(_ sender: UITextField) {
        if(txtGameJ.text == "J")
        {
            txtGameJ.isHidden = true
        }
    }
    @IBAction func txtGame3InputN(_ sender: UITextField) {
        if(txtGameN.text == "N")
        {
            txtGameN.isHidden = true
        }
    }
    @IBAction func txtGame3InputV(_ sender: UITextField) {
        if(txtGameV.text == "V")
        {
            txtGameV.isHidden = true
        }
        if(txtGameF.isHidden == true && txtGameJ.isHidden == true && txtGameN.isHidden == true && txtGameV.isHidden == true)
        {
            toGame4.isEnabled = true
        }
    }
    
    //GAME #4
    @IBOutlet weak var lblGame4E: UILabel!
    @IBOutlet weak var lblGame4O: UILabel!
    @IBOutlet weak var toGame5: UIButton!
    @IBOutlet weak var txtGame4E: UITextField!
    @IBOutlet weak var txtGame4O: UITextField!
    @IBAction func txtGame4InputE(_ sender: UITextField) {
        if(txtGame4E.text == "E")
        {
            txtGame4E.isHidden = true
            lblGame4O.text = "E"
        }
        else if(txtGame4E.text == "O")
        {
            txtGame4E.isHidden = true
        }
    }
    @IBAction func txtGame4InputO(_ sender: UITextField) {
        if(txtGame4O.text == "E")
        {
            txtGame4O.isHidden = true
        }
        else if(txtGame4O.text == "O")
        {
            txtGame4O.isHidden = true
            lblGame4E.text = "O"
        }
        if(txtGame4E.isHidden == true && txtGame4O.isHidden == true)
        {
            toGame5.isEnabled = true
        }
    }
    //GAME #5
    @IBOutlet weak var txtGame5monster: UITextField!
    @IBOutlet weak var toGame6: UIButton!
    
    @IBOutlet weak var txtGame5fireball: UITextField!
    
    @IBAction func txtGame5Input1(_ sender: UITextField) {
        if(txtGame5monster.text == "3"  )
        {
            txtGame5monster.isHidden = true
        }
    }
    
    @IBAction func txtGame5Input2(_ sender: UITextField) {
        if(txtGame5fireball.text == "6")
        {
            txtGame5fireball.isHidden = true
        }
        if (txtGame5fireball.isHidden == true && txtGame5monster.isHidden == true)
        {
            toGame6.isEnabled = true
        }
    }
    
    //GAME #6
    
    @IBOutlet weak var txtGame6Inp18: UITextField!
    @IBOutlet weak var txtGame6Inp13: UITextField!
    @IBOutlet weak var txtGame6Inp11: UITextField!
    
    
    @IBOutlet weak var txtGame6Inp29: UITextField!
    @IBOutlet weak var txtGame6Inp24: UITextField!
    @IBOutlet weak var txtGame6Inp23: UITextField!
    
    @IBOutlet weak var txtGame6Inp37: UITextField!
    @IBOutlet weak var txtGame6Inp34: UITextField!
    @IBOutlet weak var txtGame6Inp32: UITextField!
    
    
    @IBOutlet weak var toSelesai: UIButton!
    
    @IBAction func txtGame61_8(_ sender: UITextField) {
        if (txtGame6Inp18.text == "8")
        {
            txtGame6Inp18.isHidden = true
        }
    }
    
    @IBAction func txtGame61_3(_ sender: UITextField) {
        if (txtGame6Inp13.text == "3")
        {
            txtGame6Inp13.isHidden = true
        }
    }
    @IBAction func txtGame61_1(_ sender: UITextField) {
        if (txtGame6Inp11.text == "1")
        {
            txtGame6Inp11.isHidden = true
        }
    }
    @IBAction func txtGame62_9(_ sender: UITextField) {
        if (txtGame6Inp29.text == "9")
        {
            txtGame6Inp29.isHidden = true
        }
    }
    @IBAction func txtGame62_4(_ sender: UITextField) {
        if (txtGame6Inp24.text == "4")
        {
            txtGame6Inp24.isHidden = true
        }
    }
    @IBAction func txtGame62_3(_ sender: UITextField) {
        if (txtGame6Inp23.text == "3")
        {
            txtGame6Inp23.isHidden = true
        }
    }
    @IBAction func txtGame63_7(_ sender: UITextField) {
        if (txtGame6Inp37.text == "7")
        {
            txtGame6Inp37.isHidden = true
        }
    }
    @IBAction func txtGame63_4(_ sender: UITextField) {
        if (txtGame6Inp34.text == "4")
        {
            txtGame6Inp34.isHidden = true
        }
    }
    @IBAction func txtGame63_2(_ sender: UITextField) {
        if (txtGame6Inp32.text == "2")
        {
            txtGame6Inp32.isHidden = true
        }
        if ( txtGame6Inp18.isHidden == true && txtGame6Inp13.isHidden == true && txtGame6Inp11.isHidden == true && txtGame6Inp29.isHidden == true && txtGame6Inp24.isHidden == true && txtGame6Inp23.isHidden == true && txtGame6Inp37.isHidden == true && txtGame6Inp34.isHidden == true && txtGame6Inp32.isHidden == true )
        {
            toSelesai.isEnabled = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

